#!/usr/bin/python3

#############################################
# Calc. UpperLimit w/ random aperture       #
# Made by R. Itoh (2018-03-19)              #
# Last update 2018-03-19                    #
#############################################

# ===============================================
# Imports  ======================================
# ===============================================

import sys,os,time,math,random,re
import urllib,getopt,string,optparse
import numpy
import astropy
import astropy.stats
import astropy.coordinates
import pyraf,iraf


# ===============================================
# Class UpperLimit  =============================
# ===============================================

class UpperLimit():

    def __init__(self,fits):

        if ".fits" not in fits:
            print(fits,"is not FITS file (use .fits extension)")
            return False
        
        pw_UCAC  = "./"
        if os.path.exists(pw_UCAC)==False:
            print("No UCAC-4 catalog")
            return False
        sys.path.append(pw_UCAC)

        self.fits      = fits

        self.skyoffset = 5.0 # inner sky radius = self.aperture + self.skyoffset
        self.sky_width = 3.0 # outer sky radius = self.aperture + self.skyoffset + self.sky_width
        self.sigma     = 5.0 
        self.num_aper  = 2000
        self.th_skyflx = 5.0
        self.FoVratio  = 0.9
        self.sextract  = "/usr/bin/sextractor"
        self.sexconv   = "default.conv"  # parameter file will be made by sextract_conv()
        self.sexparam  = "default.param" # parameter file will be made by sextract_param()
        self.sextmp    = "test.cat"      # default name of outputfile made by sextractor

        self.read_fits_header()
        
    def read_fits_header(self):
        
        fd   = astropy.io.fits.open(self.fits)
        hd   = fd[0].header  
        xmax = fd[0].header["NAXIS1"]
        ymax = fd[0].header["NAXIS2"]
        cx = xmax/2.0
        cy = ymax/2.0
        twcs   = astropy.wcs.WCS(self.fits)
        cpos   = twcs.all_pix2world([cx],[cy],1)
        self.RA,self.Dec = cpos[0][0],cpos[1][0]
        
        if "FIL-ZMG" in  hd:self.fil = fd[0].header["FIL-ZMG"]
        else:
            if "FILTER" in hd:
                tmpfil = fd[0].header["FILTER"]
                if "Clear" not in tmpfil:
                    self.fil = tmpfil.replace(" ","")
                else:
                    if   "FIL-PHOT" in hd:self.fil = fd[0].header["FIL-PHOT"]
                    elif "FIL-NAME" in hd:self.fil = fd[0].header["FIL-NAME"]
                    else:self.fil = "r"
            else:
                if   "FIL-PHOT" in hd:self.fil = fd[0].header["FIL-PHOT"]
                elif "FIL-NAME" in hd:self.fil = fd[0].header["FIL-NAME"]
                else:self.fil = "r"
        if self.fil == "BLANK":
            self.fil = "r"
            
        self.NC   = 1.0
        if 'GAIN'     in fd[0].header:self.gainflag = True
        else:                         self.gainflag = False
        self.minX = (1.0-self.FoVratio)*fd[0].header["NAXIS1"]
        self.maxX =      self.FoVratio *fd[0].header["NAXIS1"]
        self.minY = (1.0-self.FoVratio)*fd[0].header["NAXIS2"]
        self.maxY =      self.FoVratio *fd[0].header["NAXIS2"]
        fd.close()

    def sextract_param(self):
        k = open(self.sexparam,"w")
        k.write("X_IMAGE Object position along x [pixel]\n")
        k.write("Y_IMAGE Object position along y [pixel]\n")
        k.write("FWHM_IMAGE FWHM assuming a gaussian core [pixel]\n")
        k.close()
 
        
    def sextract_conv(self):
        k = open(self.sexconv,"w")
        k.write("CONV NORM\n")
        k.write("1 2 1\n")
        k.write("2 4 2\n")
        k.write("1 2 1\n")
        k.close()
 

    def set_mode(self):
        if self.mode == "fwhm":
            self.aperture  = 1.25*self.fwhm # aperture radius for self.fixapp != "fwhm" mode

    def make_outputfilename(self):

        self.posf  = self.fits.replace(".fits","_pos.dat")
        self.rp    = self.fits.replace(".fits","_randompos_cor.dat")
        
    def calc_cone_dis(self,RA1_1,DEC1_1,RA1_2,DEC1_2):
        x1_1 = math.cos(math.radians(DEC1_1))*math.cos(math.radians(RA1_1))
        y1_1 = math.cos(math.radians(DEC1_1))*math.sin(math.radians(RA1_1))
        z1_1 = math.sin(math.radians(DEC1_1))
        x1_2 = math.cos(math.radians(DEC1_2))*math.cos(math.radians(RA1_2))
        y1_2 = math.cos(math.radians(DEC1_2))*math.sin(math.radians(RA1_2))
        z1_2 = math.sin(math.radians(DEC1_2))
        d1   = math.sqrt( (x1_1-x1_2)**2 + (y1_1-y1_2)**2 + (z1_1-z1_2)**2 )
        the1 = math.degrees(2*math.asin(d1/2.0))
        return the1
    
    def sdss_r2R(self,r,i): #Lupton (2005)
        # vega 2 AB
        R = r - 0.2936*(r - i) - 0.1439 + 0.21
        return R
    
    def sdss_i2I(self,r,i): #Lupton (2005)
        I = r - 1.2444*(r - i) - 0.3820 + 0.45
        return I

    def make_compf(self,NS=1000,minmag=10.0,maxmag=18.0,Rad=0.3):
        self.compf = self.fits.replace(".fits","_compf.dat")
        ns   = {"B":7,"V":3,"g":10,"r":8,"i":9,
                "J":4,"H":5,"Ks":6}
        import UCAC4_def
        CAT    = UCAC4_def.search_CAT_UCAC4(self.RA,self.Dec,float(Rad))
        NL1    = [[],[]]
        pNL1   = []
        for i in range(len(CAT[0])):
            if float(minmag) < CAT[ns["V"]][i] and CAT[ns["V"]][i] < float(maxmag):
                ra   = float(CAT[1][i])
                dec  = float(CAT[2][i])
                pNL1.append((CAT[ns["V"]][i],CAT[0][i]))
                
        sortNL1 = sorted(pNL1)
        C1      = open(self.compf,"w")
        C1.write("# From UCAC-4 Catalog\n")
        C1.write("# comp B V g r i R I J H Ks B-V\n")
        sN1     = len(pNL1)
        if sN1 >= float(NS):
            sN1  = int(NS)
        for di1 in range(sN1):
            N1     = CAT[0].index(sortNL1[di1][1])
            g,r,i  = CAT[ns["g"]][N1],CAT[ns["r"]][N1],CAT[ns["i"]][N1] ## AB magnitude
            R      = self.sdss_r2R(r,i)                                 ## AB mag
            I      = self.sdss_i2I(r,i)                                 ## AB mag
            B      = CAT[ns["B"]][N1]  -0.09 ## AB system
            V      = CAT[ns["V"]][N1]  +0.02 ## AB system
            J      = CAT[ns["J"]][N1]  +0.91 ## AB system
            H      = CAT[ns["H"]][N1]  +1.39 ## AB system
            Ks     = CAT[ns["Ks"]][N1] +1.85 ## AB system 
            BV     = B-V
            ra     = float(CAT[1][N1])
            dec    = float(CAT[2][N1])
            cor    = astropy.coordinates.SkyCoord(ra,dec,unit='deg',
                                                  frame=astropy.coordinates.FK5)
            radec  = cor.to_string('hmsdms')
            C1.write("#comp%s: UCAC-4 %s %s\n"%(di1+1,CAT[0][N1],radec))
            C1.write("comp%s %s %s %s %s %s %s %s %s %s %s %s\n"%(di1+1,
                                                                  B,V,g,r,i,R,I,
                                                                  J,H,Ks,BV))
        C1.close()
        return self.compf
    
    def read_compf(self,F1):

        P1,ra,dec = [],[],[]
        k1        = open(F1,"r")
        r1        = k1.read().split("\n")
        k1.close()
        wcs       = astropy.wcs.WCS(self.fits)
        for i in range(len(r1)):
            d1 = r1[i].split()
            if len(d1) <= 0:continue
            if "#comp" in d1[0]:
                ms     = r1[i+1].split()
                cor    = astropy.coordinates.SkyCoord(d1[3],d1[4],
                                                      frame=astropy.coordinates.FK5)
                ra.append(cor.ra.deg)
                dec.append(cor.dec.deg)
                ms.insert(0,cor.dec.deg)
                ms.insert(0,cor.ra.deg)
                P1.append(ms)
        xy = wcs.all_world2pix(ra,dec,1)
        k  = open(self.posf,"w")
        for i in range(len(xy[0])):
            k.write("%s %s\n"%(xy[0][i],xy[1][i]))        
        k.close()
        return [P1,self.posf]
    
    def read_sextcat(self,catf):
        k  = open(catf,"r")
        r  = k.read().split("\n")
        k.close()
        k2 = open(self.sexparam,"r")
        r2 = k2.read().split("\n")
        k2.close()
        params,allres = [],[]
        for x in r2:
            d = x.split()
            if len(d) != 0:
                params.append(d[0])
        for x in r:
            d = x.split()
            if "#" in d or len(d) == 0:continue
            nd = {}
            for i in range(len(params)):
                try:
                    nd[params[i]] = float(d[i])
                except:
                    nd[params[i]] = d[i]
            allres.append(nd)
        return allres
    
    def iraf2reg(self,magf):
        nL    = []
        ID    = iraf.pdump(magf,"ID",expr="yes",Stdout=1)
        X     = iraf.pdump(magf,"XCENTER",expr="yes",Stdout=1)
        Y     = iraf.pdump(magf,"YCENTER",expr="yes",Stdout=1)
        mag   = iraf.pdump(magf,"MAG",expr="yes",Stdout=1)
        err   = iraf.pdump(magf,"MERR",expr="yes",Stdout=1)
        Sky   = float(iraf.pdump(magf,"ANNULUS",expr="yes",Stdout=1)[0])
        Sky_r = float(iraf.pdump(magf,"DANNULUS",expr="yes",Stdout=1)[0])
        TASK  = iraf.pdump(magf,"TASK",expr="yes",Stdout=1)[0]
        if TASK == "phot":
            Apature = float(iraf.pdump(magf,"APERTURES",expr="yes",Stdout=1)[0])
            if ".iraf" in magf:outreg  = magf.replace(".iraf",".reg")
            else:outreg  = "result_apphot.reg"
        else:
            print("Unsupported TASK")
            return False
        # write region file
        k1 = open(outreg,"w")
        k1.write('global color=green font="helvetica 10 normal" ')
        k1.write('select=1 highlite=1 edit=1 move=1 delete=1 ')
        k1.write('include=1 fixed=0 source\nimage\n')
        for di1 in range(len(ID)):
            k1.write("circle(%s,%s,%s) # text={%s}\n" %(X[di1],
                                                        Y[di1],
                                                        Apature,
                                                        ID[di1]))
            k1.write("circle(%s,%s,%s) #\n"%(X[di1],Y[di1],Sky))
            k1.write("circle(%s,%s,%s) #\n"%(X[di1],Y[di1],Sky+Sky_r))
        k1.close()


    def phot_ap(self,name,pos,centroid=True):
        mag = self.fits.replace(".fits","_%s_app%s.iraf"%(name,str(int(self.aperture))))
        if os.path.exists(mag):os.system('rm %s'%mag)
        iraf.noao()
        iraf.digiphot(_doprint=0)
        iraf.apphot(_doprint=0)
        #set parameters
        #print iraf.noao.digiphot.apphot.phot.datapars.gain
        iraf.noao.digiphot.apphot.phot.datapars.noise            = 'poisson'
        if self.gainflag:iraf.noao.digiphot.apphot.phot.datapars.gain = 'GAIN'
        iraf.noao.digiphot.apphot.phot.datapars.ccdread          = 'RONOISE'
        if centroid:
            iraf.noao.digiphot.apphot.phot.centerpars.calgorithm = 'centroid'
            iraf.noao.digiphot.apphot.phot.centerpars.cbox       = 5
            iraf.noao.digiphot.apphot.phot.fitskypars.salgorithm = 'centroid'
        else:
            iraf.noao.digiphot.apphot.phot.centerpars.calgorithm = 'none'
        iraf.noao.digiphot.apphot.phot.verify                    = 'no'
        iraf.noao.digiphot.apphot.phot.interactive               = 'no'
        iraf.noao.digiphot.apphot.phot.wcsin                     = 'logical'
        iraf.noao.digiphot.apphot.phot.wcsout                    = 'logical'
        
        iraf.noao.digiphot.apphot.phot.fitskypars.annulus        = self.aperture+self.skyoffset
        iraf.noao.digiphot.apphot.phot.photpars.apertur          = self.aperture
        iraf.noao.digiphot.apphot.phot.fitskypars.dannulus       = self.sky_width
        iraf.noao.digiphot.apphot.phot.photpars.weighting        = 'constant'
        # phot
        print(self.fits,pos,mag)
        iraf.noao.digiphot.apphot.phot(image=self.fits,coords=pos,output=mag)
        return mag
    
    def find_allstar(self):
        cat = self.fits.replace(".fits",".cat")
        self.sextract_param()
        self.sextract_conv()
        os.system(self.sextract+" "+self.fits+" >> ~/sextract_log.dat")
        if os.path.exists(self.sextmp)==False:return [[],[]]
        os.system("mv %s %s"%(self.sextmp,cat))
        resL = self.read_sextcat(cat)
        cl_wcs,cl_ps = [],[]
        allFWHM= []
        wcs    = astropy.wcs.WCS(self.fits)
        for res in resL:
            ra,dec = wcs.all_pix2world(res["X_IMAGE"],res["Y_IMAGE"],0)
            cl_wcs.append([ra,dec])
            cl_ps.append([res["X_IMAGE"],res["Y_IMAGE"]])
            allFWHM.append(res["FWHM_IMAGE"])
        self.fwhm = numpy.median(allFWHM)
        return [cl_wcs,cl_ps]

    def make_random_pos(self):
        ps   = self.find_allstar()
        self.set_mode()
        k    = open(self.rp,"w")
        cnt  = 0
        apos = []
        for tr in range(int(10*self.num_aper)):
            x    = random.uniform(self.minX,self.maxX)
            y    = random.uniform(self.minY,self.maxY)
            flag = True
            for xy in ps[1]:
                if math.sqrt((xy[0]-x)**2+(xy[1]-y)**2) < 10:
                    flag = False
                    break
            for p in apos:
                if  math.sqrt((p[0]-x)**2+(p[1]-y)**2) < 2*self.aperture:
                    flag = False
                    break
            if flag:
                k.write("%s %s\n"%(x,y))
                apos.append([x,y])
                cnt += 1
            if cnt > float(self.num_aper):break
        k.close()
        return self.rp

    def sig_cut_flx(self,fluxList):
        skyval = []
        for fl in fluxList:
            if fl == "INDEF":continue
            skyval.append(self.NC*float(fl))
        rnsky2 = []
        sig1   = numpy.std(skyval)
        #med1  = scipy.stats.mode(rnsky)[0]
        med1   = numpy.median(skyval)
        for fl in skyval:
            if abs(fl-med1) < self.th_skyflx*sig1:
                rnsky2.append(fl)
        return rnsky2

    def random_upperlimit(self,flcut="off"):

        self.make_outputfilename()
        flx2   = self.fits.replace(".fits","_random_uplim.dat")
        sigs   = self.fits.replace(".fits","_uplim_sigmas.dat")
        rp     = "random_pos.dat"
        ns     = {"B":3,"V":4,"G":5,"g":5,"r":6,
                  "i":7,"R":8,"I":9,
                  "Red":8,"Green":4,"Blue":3,"unknown":8,"Lunar":8,
                  "J":10,"H":11,"Ks":12}
        if self.compf=="none":self.compf = self.make_compf()
        rp_file = self.make_random_pos()
        comp    = self.read_compf(self.compf)
        mag     = self.phot_ap("comparison",comp[1])
        flux    = iraf.pdump(mag,"FLUX",expr="yes",Stdout=1)
        cID     = iraf.pdump(mag,"ID",  expr="yes",Stdout=1)

        magran  = self.phot_ap("random",rp_file,centroid=False)
        regf    = self.iraf2reg(magran)
        ranfl   = iraf.pdump(magran,"FLUX",expr="yes",Stdout=1)


        while True:
            nf = self.sig_cut_flx(ranfl)
            print(astropy.stats.mad_std(nf))
            if len(nf) == len(ranfl):
                rnsky = nf
                break
            ranfl = nf
        
        w1      = open(sigs,"w")
        for s in rnsky:
            w1.write("%s\n"%s)
        w1.close()
        sig1    = numpy.std(rnsky)
        print("sig",sig1)
        allUp   = []
        zeromag = []
        kfl2    = open(flx2,"w")
        kfl2.write("#Band:%s\n"%self.fil)
        kfl2.write("#Apetur:%s pixel\n"%self.aperture)
        for cid in cID:
            i = cID.index(cid)
            if flux[i] == "INDEF": continue
            if float(flux[i]) <= 0:continue
            Up   = -2.5*math.log((self.sigma*sig1)/(self.NC*float(flux[i])),10)+float(comp[0][int(i-1)][ns[self.fil]])
            kfl2.write("%s %s-sigma comp%s %s %s\n"%(self.fits,self.sigma,cid,self.fil,Up))
            print("%s %s-sigma comp%s %s %s"  %(self.fits,self.sigma,cid,self.fil,Up))
            try:
                zm =  float(comp[0][int(i-1)][ns[self.fil]]) + 2.5*math.log(float(flux[i]),10)
                zeromag.append(zm)
            except:
                pass
            allUp.append(Up)
        try:
            self.med = numpy.median(allUp)
            print("%s-sig Upplim"%self.sigma,self.med)
            kfl2.write("#Median Upperlimit:%1.2f\n"%self.med)
        except:
            pass
        try:
            self.zero = numpy.median(zeromag)
            print("zeromag",self.zero)
        except:
            self.zero = 50
            pass

        
        kfl2.close()


        
        
        
if __name__ == "__main__":

    usage  = 'usage: %s [options] FITS\n'%sys.argv[0]
    usage += 'Calculation of Upperlimit with random aperture\n'
    usage += 'Need FITS file'
    parser = optparse.OptionParser(usage=usage)
    parser.set_defaults(verbose=True)
    parser.add_option("-a", "--aperture",
                      default = "none",
                      help    = "aperture size [pixel]")
    parser.add_option("-s", "--sigma",
                      default = 5.0,
                      help    = "significance for Upperlimit [default: %default]")
    parser.add_option("-c", "--comparison",
                      default = "none",
                      help    = "Comparison text file [default: %default]")
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    (opts, args)    = parser.parse_args()

    fits = sys.argv[1]
    UL   = UpperLimit(fits)
    
    if opts.aperture != "none":
        UL.mode       = "fixapp"
        UL.aperture   = float(opts.aperture)
    else:UL.mode      = "fwhm"
    UL.sigma    = float(opts.sigma)
    UL.compf    = opts.comparison
    
    UL.random_upperlimit()
