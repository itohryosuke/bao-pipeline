#!/usr/bin/python3

import pyraf,iraf
import sys,os,time,math,random,re
import urllib,getopt,string,optparse,numpy
import astropy
import astropy.io.fits
import astropy.coordinates
import astropy.stats
import logging
import lacosmic
import ephem
import astropy.coordinates.name_resolve

pw_UCAC  = "Input_UCAC4_tool_dir/"
sys.path.append(pw_UCAC)

import reduction_stl
import judge_filter
import phot_all
import calc_upperlimit


# Input FITS file list (should be plain text)
alllistfile = "list_all_STL_fits.txt"

# output file name
resfile     = "BAO_STL_allresults.txt"

analysisdir = "./"
logf  = "log.dat"

def judge_caldata(filename):
    cal = ["DARK","FLAT","DOME","DOMEFLAT","DFLAT","BIAS"]
    con = re.split('[,._-]',filename)
    for c in con:
        for cand in cal:
            if cand in c.upper():
                return False
    return True

def reduction(infits):
    rd.check_reducer(infits)
    rd.check_filter_by_name(infits)
    ra,dec = find_objname(infits)
    dk  = rd.master_dark_subtraction(fits)
    fl  = rd.master_flat_correction(dk)
    os.system("rm -f %s"%dk)
    lc  = rd.remove_cosmic(fl)
    os.system("rm -f %s"%fl)
    rd.write_fil_red(lc)
    jd = rd.count_star_num(lc)
    if jd==False:return False
    cnt = 1
    rd.ra = ra
    rd.dec= dec
    while cnt <= rd.wcstrialNum:
        wcs = rd.wcs_astrometry(lc)
        if wcs:break
        cnt += 1
    if wcs==False:
        rd.ra = ra
        rd.dec= dec
        if rd.Rmode == "wo":
            rd.Rmode   = "w"
            rd.Pscale  = [0.8,0.9] # 0.8 -- 0.9 arcsec/pixel
            rd.rad     = 0.30      # ROI [degree]
        else:
            rd.Rmode   = "wo"
            rd.Pscale  = [0.3,0.5] # 0.3 -- 0.5 arcsec/pixel
            rd.rad     = 0.15      # ROI [degree]
        cnt = 1
        while cnt <= rd.wcstrialNum:
            wcs = rd.wcs_astrometry(lc)
            if wcs:
                rd.write_fil_red(wcs)
                break
            cnt += 1
    rd.rm_wcstempfile(lc)
    os.system("rm -f %s"%lc)
    return wcs

def judge_band(infits):
    print(infits)
    jd   = judge_filter.Judge_Filter(infits)
    ph   = phot_all.Phot(infits)
    jd.make_comp()
    ph.mes_fwhm()
    fwhm = ph.fwhm
    cat  = ph.calc_zeromag()
    cat  = ph.phot_allstar()
    band = jd.judge(cat)
    std  = jd.std_min
    starnum = jd.star_num
    os.system("/usr/bin/sethead %s FIL-PHOT=%s"%(infits,band))
    ph   = phot_all.Phot(infits)
    ph.fwhm = fwhm
    cat     = ph.calc_zeromag()
    return band,std,fwhm,starnum

def upperlimit(infits):
    ul = calc_upperlimit.UpperLimit(infits)
    ul.mode     = "fwhm"
    ul.sigma    = 5.0
    ul.compf    = "none"
    ul.random_upperlimit()
    return ul.med

def sky_brightness(infits):
    fd  = astropy.io.fits.open(infits)
    D   = fd[0].data
    x,y = D.shape    
    br  = numpy.median(D)
    c1  = fd[0].header["CD1_1"]
    c2  = fd[0].header["CD1_2"]
    r   = 0.7
    pixscale = math.sqrt(c1**2+c2**2)*3600.0 # arcsec/pix
    zmag  = fd[0].header["BAO-ZMG"]
    tbr   = br/pixscale**2
    skymag   = -2.5*math.log(tbr,10)+zmag
    fd.close()
    return skymag,br,pixscale

def calc_cone_dis(RA1_1,DEC1_1,RA1_2,DEC1_2):
    x1_1 = math.cos(math.radians(DEC1_1))*math.cos(math.radians(RA1_1))
    y1_1 = math.cos(math.radians(DEC1_1))*math.sin(math.radians(RA1_1))
    z1_1 = math.sin(math.radians(DEC1_1))
    x1_2 = math.cos(math.radians(DEC1_2))*math.cos(math.radians(RA1_2))
    y1_2 = math.cos(math.radians(DEC1_2))*math.sin(math.radians(RA1_2))
    z1_2 = math.sin(math.radians(DEC1_2))
    d1   = math.sqrt( (x1_1-x1_2)**2 + (y1_1-y1_2)**2 + (z1_1-z1_2)**2 )
    the1 = math.degrees(2*math.asin(d1/2.0))
    return the1

def convert_radec2altaz(ra,dec,mjd,lon,lat):
    lst   = UTC2LST(mjd,lon)
    ha    = calc_HA(ra,lst)
    a1,a2 = calc_altaz(ha,dec,lat)
    return a1,a2

def UTC2LST(mjd,lon):
    jd   = mjd+ 2400000.5
    HG1  = 24*(0.671262+1.0027379094*(jd-2440000.5))
    return (HG1 - math.floor(HG1/24.0)*24.0) + lon/15.0

def calc_HA(ra1,lst1):
    dlst1  = lst1 - ra1/15.0
    while 24 < dlst1:
        dlst1 += -24.0
    while dlst1 < 0:
        dlst1 += 24.0
    return dlst1

def calc_altaz(ha1,dec1,lat):
    d1   = math.radians(lat) 
    d2   = math.radians(dec1)
    d3   = math.radians(ha1*15.0)
    sinh = math.sin(d1)*math.sin(d2)+math.cos(d1)*math.cos(d2)*math.cos(d3)  
    cosA = math.cos(d1)*math.sin(d2)-math.sin(d1)*math.cos(d2)*math.cos(d3)
    alt  = math.degrees(math.asin(sinh))
    az   = math.degrees(math.acos(cosA/math.cos(math.radians(alt))))
    if ha1 < 12.0:
        az = -1*az

    while az < 0:
        az += 360.0
    while az > 360.0:
        az -= 360.0
    return alt,az   



def read_info(infits):
    rkey = ["FILENAME",
            "DATE","MJD","EXPOSURE","CCDTMP",
            "RA","DEC","ALT","AZ",
            "PIXEL-SCALE","REDUCER","FLATTEN",
            "WCS-MATCH","MATCH-STD",
            "FILTER-ORG",
            "FILTER-NAME",
            "FILTER-PHOT",
            "ZEROMAG-FILTER",
            "ZEROMAG-VALUE",
            "FWHM",
            "UPPERLIMIT",
            "SKY-MAG",
            "SKY-COUNT",
            "SUN_RA","SUN_DEC","SUN_ALT",
            "MOON_RA","MOON_DEC","MOON_ALT","MOON_AZ",
            "MOON_AGE","MOON_SEPARATION"]
    res = {}
    for k in rkey:res[k] = -999

    fd = astropy.io.fits.open(infits)
    hd = fd[0].header

    res["DATE"]     = hd["DATE-OBS"]
    res["EXPOSURE"] = hd["EXPTIME"]
    res["CCDTMP"]   = hd["CCD-TEMP"]+273.15
    res["RA"]       = hd["CRVAL1"]
    res["DEC"]      = hd["CRVAL2"]
    res["REDUCER"]  = hd["RED-VIG"]
    res["FLATTEN"]  = hd["FLATTEN"]
    res["FILTER-ORG"]  = hd["FILTER"]
    res["FILTER-NAME"] = hd["FIL-NAME"]
    res["FILTER-PHOT"] = hd["FIL-PHOT"]
    res["ZEROMAG-VALUE"]  = hd["BAO-ZMG"]
    res["ZEROMAG-FILTER"] = hd["FIL-ZMG"]    
    fd.close()
    #calc ephem
    bao     = ephem.Observer()
    bao.lon = '133.5460'
    bao.lat = '34.6721'
    bao.elevation = 420
    p          = res["DATE"].split(".")[0].split("T")
    bao.date   = "%s %s"%(p[0],p[1])
    mjd        = ephem.julian_date(bao.date)- 2400000.5
    res["MJD"] = mjd


    sun      = ephem.Sun(bao)
    moon     = ephem.Moon(bao)
    res["SUN_RA"]   = math.degrees(sun.ra)
    res["SUN_DEC"]  = math.degrees(sun.dec)
    res["SUN_ALT"]  = math.degrees(sun.alt)
    res["SUN_AZ"]   = math.degrees(sun.az)
    res["MOON_RA"]  = math.degrees(moon.ra)
    res["MOON_DEC"] = math.degrees(moon.dec)
    res["MOON_ALT"] = math.degrees(moon.alt) 
    res["MOON_AZ"]  = math.degrees(moon.az)
    res["MOON_AGE"] = bao.date - ephem.previous_new_moon(bao.date)
    res["MOON_SEPARATION"] = calc_cone_dis(res["RA"],res["DEC"],moon.ra,moon.dec)

    alt,az = convert_radec2altaz(res["RA"],res["DEC"],mjd,
                                 math.degrees(bao.lon),
                                 math.degrees(bao.lat))
    res["ALT"] = alt
    res["AZ"]  = az    
    return res,rkey

def rm_fits(fitsname):

    bn = os.path.splitext(fitsname)[0]

    cmd  = "rm -f " + bn + "*"
    os.system(cmd)
    print(cmd)

def judge_float(val):
    try:
        float(val)
        return True
    except:
        return False
    
def find_objname(fitsname):
    con = re.split('[,._-]',fitsname)
    tar = con[0]
    mm  = ""
    for i in range(len(tar)):

        if i == 0:
            mm += tar[i]
            continue
        elif judge_float(tar[i-1]) and judge_float(tar[i]) == False:
            break
        mm += tar[i]
    print(mm)
    try:
        pos = astropy.coordinates.name_resolve.get_icrs_coordinates(mm)
        return pos.ra.deg,pos.dec.deg
    except:
        return False,False


    
    
k     = open(resfile,"r")
r     = k.read().split("\n")
afits = []
for l in r:
    d = l.split(",")
    if len(d) == 0:continue
    afits.append(d[0])
k.close()

k       = open(alllistfile,"r")
allfits = k.read().split()
k.close()

rd = reduction_stl.reduction()




for orgfits in allfits:
    w    = open(resfile,"a")
    log  = open(logf,"a")
    fits = os.path.basename(orgfits)
    if judge_caldata(fits) == False:continue
    if orgfits in afits:continue
    os.system("cp %s %s"%(orgfits,fits))

    try:
        wcs = reduction(fits)
    except:
        w.write("%s\n"%orgfits)
        log.write("%s,%s,%s\n"%(orgfits,"reduction",sys.exc_info()[1]))
        rm_fits(fits)
        continue
    if wcs == False:
        w.write("%s\n"%orgfits)
        log.write("%s,%s\n"%(orgfits,"wcs match"))
        rm_fits(fits)
        continue
    try:
        band,std,fwhm,SN    = judge_band(wcs)
        uplim               = upperlimit(wcs)
        skymag,skycnt,pxscl = sky_brightness(wcs)
    except:
        w.write("%s\n"%orgfits)
        log.write("%s,%s,%s\n"%(orgfits,"analysis",sys.exc_info()[1]))
        rm_fits(fits)
        continue
    res,key = read_info(wcs)
    res["FILENAME"]    = orgfits
    res["PIXEL-SCALE"] = pxscl
    res["WCS-MATCH"]   = SN
    res["MATCH-STD"]   = std
    res["FWHM"]        = fwhm*pxscl
    res["UPPERLIMIT"]  = uplim
    res["SKY-MAG"]     = skymag
    res["SKY-COUNT"]   = skycnt
    print(res)
    for k in key:
        #print("%s,"%k,end='')
        if k == "MOON_SEPARATION":w.write("%s\n"%res[k])
        else:w.write("%s,"%res[k])
    rm_fits(fits)
    w.close()

    #sys.exit()
